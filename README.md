# Bokehy
A lazy lens bokeh effect for VRchat.

<div style="width: 5em">![Preview](https://media.discordapp.net/attachments/414634326995763201/708210485970993234/Unity_2020-05-08_16-24-17.png)</div>

## Installation
Copy the contents of the Assets folder to your Assets folder.

## Usage
The depth of field effect will only be visible in the VR hand camera, unless Desktop mode is active, in which case it will only activate when your viewpoint is at the object's centre. 

Place the shader onto a simple mesh. Then, attach the mesh to your head. The depth of field will focus on your head. However, this is very expensive for the people around you, even if they cannot see it.

Install a toggle system like
* [**EmoteSwitch**](https://booth.pm/en/items/1242826)
* [**VRCInventorySystem**](https://github.com/Xiexe/VRCInventorySystem)

Then place the mesh inside it and use the toggle system to activate it. 

(You can then stick it to your head with a Position Constraint if the system doesn't keep it there.)

The shader also requires a depth buffer to work, or it'll be unable to focus on anything. You can force the depth buffer active with an object like this:
* Directional Light
* Mode: Realtime
* Intensity: 0.01
* Shadow Type: Hard Shadows
* Culling Mask: UiMenu (very important!)

## License?

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
