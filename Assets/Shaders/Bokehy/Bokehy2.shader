﻿Shader "Silent/Bokehy/Type B"
{
    Properties
    {
        [Header(Lazy Boheh Lens)]
        [Header(Type B)]
        [Header(Settings)]
        _BlurRadiusMul("Blur radius multiplier", Float) = 1.0
        [Space]
        _NearFocus("Near Focus Offset", Float) = 0.0
        _FarFocus("Focus Area Scale", Range(0, 1)) = 0.1
        [Space]
        [Header(The focus point is the centre of the object)]
        [Toggle(_)]_UseCentreFocus("Use the centre of the screen as the focus point", Float) = 0
        [Header(The effect is only visible in the VRC camera)]
        [Toggle(_)]_DesktopMode("Desktop mode: Place on your viewpoint to activate", Float) = 0
        [Toggle(_)]_DebugMode("Debug mode: Always show effect", Float) = 0
    }
    SubShader
    {
    Tags { "Queue"="Overlay+20" }

    ZWrite Off
    ZTest Always
    Cull Off

    CGINCLUDE

    #include "UnityCG.cginc"
    #include "Bokehy.cginc"

    // Returns useable colour.
    float3 getBlurry_B(sampler2D screen, float2 uv, float focusAt,
        float blurRadius, const int MAX_BLUR_SIZE, const float RAD_SCALE,
        const bool mixMode,
        const bool useRandom = false, float random = 0 )
    {
        float vignFactor =  vignette(uv);
        
        float totalSamples = 1.0;
        float3 colAcum = tex2D(screen, uv);

        float depth = tex2D(_CameraDepthTexture, uv).r;
        depth = LinearEyeDepth(depth);

        float centerSize = getBlurSize(depth, focusAt, _FarFocus);
        
        const float GOLDEN_ANGLE = 2.39996323; 
        //const float MAX_BLUR_SIZE = 15.0; 
        //const float RAD_SCALE = 1; // Smaller = nicer blur, larger = faster

        float radius = RAD_SCALE;

        random = lerp(RAD_SCALE/MAX_BLUR_SIZE, 1.0, random);

        const float a = cos(GOLDEN_ANGLE);
        const float b = sin(GOLDEN_ANGLE);
        float s = 0.0f;
        float c = 1.0f;

        for(float angle = 0.0; radius < MAX_BLUR_SIZE; angle += GOLDEN_ANGLE) {
            float2 samplePoint = uv + float2(c, s) * (_ScreenParams.zw - 1) * radius;
            float3 sampleColor = tex2D(screen, samplePoint).rgb;

            float localDepth = depthOffset(depth, samplePoint, 0);
            
            float sampleSize = getBlurSize(localDepth, focusAt, _FarFocus)*MAX_BLUR_SIZE*blurRadius;

            if (localDepth > depth)
                sampleSize = clamp(sampleSize, 0.0, centerSize*2.0);

            sampleSize *= 1+vignFactor;

            float m = smoothstep(radius-0.5, radius+0.5, sampleSize);

            colAcum = mixMode
            ? max(colAcum, lerp(colAcum, sampleColor, m))
            : colAcum + lerp(colAcum/totalSamples, sampleColor, m);

            radius += RAD_SCALE/radius;
            totalSamples += 1.0;

            const float ns = b*c + a*s;
            const float nc = a*c - b*s;
            c = nc;
            s = ns;
        }

        colAcum = mixMode? colAcum : colAcum/totalSamples;

        return colAcum;
    }

    ENDCG

    GrabPass
    {
        "_BokehGrab1"
    }

    Pass
    {
    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag_1

    sampler2D _BokehGrab1;

    half4 frag_1(v2f i) : SV_Target
    {
        half4 bgcolor = tex2Dproj(_BokehGrab1, i.grabPos);

        float2 uv = getDepthUVs(i.grabPos);

        float vignFactor =  vignette(uv);

        float focusAt = i.props.y;

        const float blurRadMax = 0.6;
        float blurRadius = blurRadMax * _BlurRadiusMul * unity_CameraProjection._m11;

        float random = intensity(_ScreenParams.zw*i.pos+_Time.y);
        random = lerp(0.75, 1.0, random);
        
        return float4(getBlurry_B(_BokehGrab1, uv, focusAt, blurRadius, 20, 1, false), 1);
        }

        ENDCG
    }

    GrabPass
    {
        "_BokehGrab2"
    }

    Pass
    {
    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag_2

    sampler2D _BokehGrab2;

    half4 frag_2(v2f i) : SV_Target
    {
        half4 bgcolor = tex2Dproj(_BokehGrab2, i.grabPos);

        float2 uv = getDepthUVs(i.grabPos);

        float vignFactor = vignette(uv);

        float focusAt = i.props.y;

        const float blurRadMax = 0.6;
        float blurRadius = blurRadMax * _BlurRadiusMul * unity_CameraProjection._m11;
        
        float random = intensity(_ScreenParams.zw*i.pos+_Time.y);
        random = lerp(0.75, 1.0, random);
        
        return float4(getBlurry_B(_BokehGrab2, uv, focusAt, blurRadius, 20, 1, true), 1);
        return float4(bgcolor.rgb, 1.0);
    }

    ENDCG
    }
}
}
