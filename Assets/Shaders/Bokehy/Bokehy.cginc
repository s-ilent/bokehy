    struct appdata
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
    };

    struct v2f
    {
        float4 grabPos : TEXCOORD0;
        float4 pos : SV_POSITION;
        float4 props : TEXCOORD1;
    };

    // refs: 
    // https://www.shadertoy.com/view/ltKBDd
    // https://www.shadertoy.com/view/4lVfDt
    // http://blog.tuxedolabs.com/2018/05/04/bokeh-depth-of-field-in-single-pass.html
    // https://www.shadertoy.com/view/3sBXDh
    // https://www.shadertoy.com/view/wd2GWc
    // https://www.shadertoy.com/view/wdsSWS
    // https://www.shadertoy.com/view/lstBDl
    // https://www.iquilezles.org/www/articles/sincos/sincos.htm

    float _BlurRadiusMul;
    float _NearFocus;
    float _FarFocus;

    float _UseCentreFocus;
    float _DebugMode;
    float _DesktopMode;

    sampler2D _CameraDepthTexture;
    float4 _CameraDepthTexture_TexelSize;

    // Oblique projection fix for mirrors.
    // See https://github.com/lukis101/VRCUnityStuffs/blob/master/Shaders/DJL/Overlays/WorldPosOblique.shader
    #define PM UNITY_MATRIX_P

    inline float4 CalculateFrustumCorrection()
    {
        float x1 = -PM._31/(PM._11*PM._34);
        float x2 = -PM._32/(PM._22*PM._34);
        return float4(x1, x2, 0, PM._33/PM._34 + x1*PM._13 + x2*PM._23);
    }
    inline float CorrectedLinearEyeDepth(float z, float B)
    {
        return 1.0 / (z/PM._34 + B);
    }

    float2 getDepthUVs (float4 screenPos, float2 offset = 0)
    {
        float2 uv = (screenPos.xy + offset) / screenPos.w;
        #if UNITY_UV_STARTS_AT_TOP
            if (_CameraDepthTexture_TexelSize.y < 0) {
                uv.y = 1 - uv.y;
            }
        #endif
        return //uv;
        (floor(uv * _CameraDepthTexture_TexelSize.zw) + 0.5) *
        abs(_CameraDepthTexture_TexelSize.xy);
    }

    float getCentreFocus(sampler2D depthIn)
    {
        float2 offsets[7];
        offsets[0] = float2(0,    0);
        offsets[1] = float2(0.1,  0.1);
        offsets[2] = float2(0.1,  -0.1);
        offsets[3] = float2(0,    0.2);
        offsets[4] = float2(-0.1, 0.1);
        offsets[5] = float2(-0.1, -0.1);
        offsets[6] = float2(0.2,  0);

        float focus = 0;

        [unroll]
        for (int i = 0; i < 7; ++i) {
            float fSample = tex2Dlod(_CameraDepthTexture, float4(0.5+offsets[i]*0.5, 0, 0)).r;
            focus = max(focus, fSample);
        }
        return LinearEyeDepth(focus);
    }

    float getFocus()
    {
        float closeDist = UnityObjectToViewPos(float3(0, 0, 0)).z;

        return (_UseCentreFocus || _DesktopMode)
        ? getCentreFocus(_CameraDepthTexture) + _NearFocus
        : _NearFocus-closeDist;
    }

    bool isVR() {
        // USING_STEREO_MATRICES
        #if UNITY_SINGLE_PASS_STEREO
            return true;
        #else
            return false;
        #endif
    }

    bool isVRHandCamera() {
        return !isVR() && abs(UNITY_MATRIX_V[0].y) > 0.0000005;
    }

    bool isDesktop() {
        return !isVRHandCamera();
    }

    bool isVRHandCameraPreview() {
        return isVRHandCamera() && _ScreenParams.y == 720;
    }

    bool isVRHandCameraPicture() {
        return isVRHandCamera() && _ScreenParams.y == 1080;
    }

    bool isPanorama() {
        // Crude method
        // FOV=90=camproj=[1][1]
        return unity_CameraProjection[1][1] == 1 && _ScreenParams.x == 1075 && _ScreenParams.y == 1025;
    }

    v2f vert(appdata_base v) {
        v2f o;
        // use UnityObjectToClipPos from UnityCG.cginc to calculate 
        // the clip-space of the vertex
        o.pos = UnityObjectToClipPos(v.vertex);
        // use ComputeGrabScreenPos function from UnityCG.cginc
        // to get the correct texture coordinate
        o.grabPos = ComputeGrabScreenPos(o.pos);
        // Calculate some object-wide parameters here...
        // x : frustrum correction
        // y : focusing
        o.props = 0;
        o.props.x = dot(o.pos, CalculateFrustumCorrection());
        o.props.y = getFocus();

        // Disable if not in camera.
        // The 3840 check is supposed to cover Alt+Ctrl+F12 screenshots,
        // but those are not hand camera so it doesn't pass.
        bool isVisible = isVRHandCamera() 
            && (_ScreenParams.y == 1080 || _ScreenParams.y == 720 || _ScreenParams.y == 3840);

        fixed3 baseWorldPos = unity_ObjectToWorld._m03_m13_m23;
        float closeDist = distance(_WorldSpaceCameraPos, baseWorldPos);

        bool isVisibleDesktop = (abs(closeDist) < 0.25);
        isVisible = _DesktopMode? isVisibleDesktop : isVisible;

        isVisible = isVisible || _DebugMode;

        o.pos = isVisible
            ? o.pos
            : 0;

        return o;
    }

    float vignette(float2 uv)
    {
        uv = uv - 0.5;
        float vi =  sqrt(dot(uv, uv));
        //vi = vi * vi + 1;
        //vi =  1 / (vi + vi);
        vi = 1-(2*vi);
        return 1-vi;
    }

    // R dither mask
    float intensity(float2 pixel) {
        const float a1 = 0.75487766624669276;
        const float a2 = 0.569840290998;
        return frac(a1 * float(pixel.x) + a2 * float(pixel.y));
    }

    float lerpstep( float a, float b, float t)
    {
        return saturate( ( t - a ) / ( b - a ) );
    }

    float filteredDepth(float2 screenUV, float fragDepth)
    {
        float2 texelSize = _CameraDepthTexture_TexelSize.xy;
        float4 offsetDepths = 0;
        float2 uvOffsets[5] = {
            float2(1.0, 0.0) * texelSize,
            float2(-1.0, 0.0) * texelSize,
            float2(0.0, 1.0) * texelSize,
            float2(0.0, -1.0) * texelSize,
            float2(0.0, 0.0)
        };

        offsetDepths.x = tex2D(_CameraDepthTexture, screenUV + uvOffsets[0]).r;
        offsetDepths.y = tex2D(_CameraDepthTexture, screenUV + uvOffsets[1]).r;
        offsetDepths.z = tex2D(_CameraDepthTexture, screenUV + uvOffsets[2]).r;
        offsetDepths.w = tex2D(_CameraDepthTexture, screenUV + uvOffsets[3]).r;

        float4 offsetDiffs = abs(fragDepth - offsetDepths);

        float diffs[4] = {offsetDiffs.x, offsetDiffs.y, offsetDiffs.z, offsetDiffs.w};

        int lowest = 4;
        float tempDiff = 1;
        for (int i=0; i<4; i++)
        {
            if(diffs[i] < tempDiff)
            {
                tempDiff = diffs[i];
                lowest = i;
            }
        }
        return tex2D(_CameraDepthTexture, screenUV + uvOffsets[lowest]).r;
    }

    float depthOffset(float inDepth, float2 uv, float2 offset)
    {
        float depth = tex2D(_CameraDepthTexture, uv+offset).r;
        depth = LinearEyeDepth(depth);

        #if 0
        float factor = (abs(inDepth+depth));
        return factor;
        depth = tex2D(_CameraDepthTexture, uv+offset*factor).r;
        depth = LinearEyeDepth(depth);
        #endif

        #if 0
        depth = min(depth, inDepth);
        #endif

        #if 0
        float factor = (inDepth + depth);
        depth = factor > 0 ? min(depth, inDepth): max(depth, inDepth);
        #endif

        return depth;
    }

    float getBlurSize(float depth, float focusPoint, float focusScale)
    {
        float coc = clamp((1.0 / focusPoint - 1.0 / depth)*focusScale, -1.0, 1.0);
        return abs(coc);
    }