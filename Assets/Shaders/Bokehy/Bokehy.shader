﻿Shader "Silent/Bokehy/Type A"
{
    Properties
    {
        [Header(Lazy Boheh Lens)]
        [Header(Type A)]
        [Header(Settings)]
        _BlurRadiusMul("Blur radius multiplier", Float) = 1.0
        [Space]
        _NearFocus("Near Focus Offset", Float) = 0.0
        _FarFocus("Focus Area Scale", Range(0, 1)) = 0.1
        [Space]
        [Header(The focus point is the centre of the object)]
        [Toggle(_)]_UseCentreFocus("Use the centre of the screen as the focus point", Float) = 0
        [Header(The effect is only visible in the VRC camera)]
        [Toggle(_)]_DesktopMode("Desktop mode: Place on your viewpoint to activate", Float) = 0
        [Toggle(_)]_DebugMode("Debug mode: Always show effect", Float) = 0
    }
    SubShader
    {
    Tags { "Queue"="Overlay+20" }

    ZWrite Off
    ZTest Always
    Cull Off

    CGINCLUDE

    #include "UnityCG.cginc"
    #include "Bokehy.cginc"

    // Returns useable colour.
    float3 getBlurry_A(sampler2D screen, float2 uv, float focusAt,
        float blurRadius, const float blurCircles = 4.0,
        const bool mixMode = false, 
        const bool useRandom = false, float random = 1 )
    {
        float vignFactor =  vignette(uv);
        
        float totalSamples = 0.0;
        float3 colAcum = 0;

        float depth = tex2D(_CameraDepthTexture, uv).r;
        depth = LinearEyeDepth(depth);

        float centerSize = getBlurSize(depth, focusAt, _FarFocus);

        float angle = (2.0 * UNITY_PI);

        float s = 0.0f;
        float c = 1.0f;

        for(float currentCircle = 0.0; currentCircle < blurCircles; currentCircle++) {
            float samplesForCurrentCircle = (pow((currentCircle + 1.0), 2.0) - pow(currentCircle, 2.0)) * 4.0;
            float currentRadius = (blurRadius / float(blurCircles)) * (float(currentCircle) + 0.5);
            currentRadius *= random;
            float currentAngle = angle / samplesForCurrentCircle;
            const float a = cos(currentAngle);
            const float b = sin(currentAngle);
              
            for(float currentSample = 0.0; currentSample < samplesForCurrentCircle; currentSample++) {
                float2 samplePoint = float2(0.0, currentRadius);

                float2x2 m = float2x2(c, -s, s, c);
                samplePoint = mul(m, samplePoint);

                float localDepth = depthOffset(depth, uv, samplePoint);
                
                float sampleSize = getBlurSize(localDepth, focusAt, _FarFocus);

                if (localDepth > depth)
                    sampleSize = clamp(sampleSize, 0.0, centerSize*2.0);

                sampleSize *= 1+vignFactor;

                sampleSize = smoothstep(0, 1, sampleSize);

                samplePoint *= sampleSize;
                
                samplePoint *= float2(_ScreenParams.y / _ScreenParams.x, 1.0);

                totalSamples++;

                colAcum = mixMode
                ? max(colAcum, tex2D(screen, uv + samplePoint).rgb)
                : colAcum + tex2D(screen, uv + samplePoint).rgb;

                const float ns = b*c + a*s;
                const float nc = a*c - b*s;
                c = nc;
                s = ns;
            }
        }

        colAcum = mixMode? colAcum : colAcum/totalSamples;

        return colAcum;
    }

    ENDCG

    GrabPass
    {
        "_BokehGrab1"
    }

    Pass
    {
    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag_1

    sampler2D _BokehGrab1;

    half4 frag_1(v2f i) : SV_Target
    {
        half4 bgcolor = tex2Dproj(_BokehGrab1, i.grabPos);

        float2 uv = getDepthUVs(i.grabPos);

        float vignFactor =  vignette(uv);

        float focusAt = i.props.y;

        fixed3 baseWorldPos = unity_ObjectToWorld._m03_m13_m23;
        float closeDist2 = distance(_WorldSpaceCameraPos, baseWorldPos);

        float blurRadMax = 0.008;
        float blurRadius = blurRadMax * _BlurRadiusMul * unity_CameraProjection._m11;

        float random = intensity(_ScreenParams.zw*i.pos+_Time.y);
        random = lerp(0.5, 1.0, random);
        
        return float4(getBlurry_A(_BokehGrab1, uv, focusAt, blurRadius, 4, false), 1);
        }

        ENDCG
    }

    GrabPass
    {
        "_BokehGrab2"
    }

    Pass
    {
    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag_2

    sampler2D _BokehGrab2;

    half4 frag_2(v2f i) : SV_Target
    {
        half4 bgcolor = tex2Dproj(_BokehGrab2, i.grabPos);

        float2 uv = getDepthUVs(i.grabPos);

        float vignFactor = vignette(uv);

        float focusAt = i.props.y;

        fixed3 baseWorldPos = unity_ObjectToWorld._m03_m13_m23;
        float closeDist2 = distance(_WorldSpaceCameraPos, baseWorldPos);

        float blurRadMax = 0.008;
        float blurRadius = blurRadMax * _BlurRadiusMul * unity_CameraProjection._m11;

        return float4(getBlurry_A(_BokehGrab2, uv, focusAt, blurRadius, 4, true), 1);
        return float4(bgcolor.rgb, 1.0);
        }

        ENDCG
    }
}
}
